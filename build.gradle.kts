// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
	repositories {
		gradlePluginPortal()
		mavenCentral()
		maven { setUrl(System.getProperty("user.home")+".android/manual-offline-m2/gradle/repository")  }
		google()
	}
	dependencies {
		classpath("com.android.tools.build:gradle:4.1.3")
		classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${ProjectConfig.KOTLIN_VERSION}")

		// NOTE: Do not place your application dependencies here; they belong
		// in the individual module build.gradle files
	}
}

allprojects {
	repositories {
		mavenCentral()
		google()
		maven { setUrl("${rootProject.projectDir}/gradle/repository")  }
		maven { setUrl(System.getProperty("user.home")+".android/manual-offline-m2/gradle/repository")  }
	}
}

tasks.register("clean", Delete::class) {
	delete(rootProject.buildDir)
}