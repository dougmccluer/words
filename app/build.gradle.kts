plugins {
	id("com.android.application")
	kotlin("android")
	kotlin("kapt")
}

android {
	compileSdkVersion(30)
	buildToolsVersion = "30.0.2"

	defaultConfig {
		applicationId = "com.dougmccluer.words"
		minSdkVersion(21)
		targetSdkVersion(30)
		versionCode = 1
		versionName = "1.0"
		javaCompileOptions.annotationProcessorOptions {
			arguments["room.schemaLocation"] = "$projectDir/schemas"
		}
		testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
	}

	buildTypes {
		named("release").configure {
			isMinifyEnabled = false
			proguardFiles(
				getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro"
			)
		}
	}
	compileOptions {
		sourceCompatibility = JavaVersion.VERSION_1_8
		targetCompatibility = JavaVersion.VERSION_1_8
		isCoreLibraryDesugaringEnabled = true
	}
	kotlinOptions {
		jvmTarget = "1.8"
		languageVersion = "1.4"
		useIR = false
	}
}

dependencies {
	coreLibraryDesugaring("com.android.tools:desugar_jdk_libs:1.1.5")

	implementation( "org.jetbrains.kotlin:kotlin-stdlib:${ProjectConfig.KOTLIN_VERSION}")
	implementation( "org.jetbrains.kotlinx:kotlinx-coroutines-android:1.4.3")
	implementation( "androidx.core:core-ktx:1.3.2")
	implementation( "androidx.appcompat:appcompat:1.2.0")
	implementation( "com.google.android.material:material:1.3.0")
	implementation( "androidx.constraintlayout:constraintlayout:2.0.4")

	//koin - dependency injection
	val koin_version = "3.0.1"
	implementation("io.insert-koin:koin-android:$koin_version")
	implementation( "io.insert-koin:koin-androidx-compose:$koin_version")

	//OKHTTP & Retrofit - HTTP request management
	implementation("com.squareup.okhttp3:okhttp:4.9.0")
	implementation("com.squareup.okhttp3:logging-interceptor:4.9.0")
	implementation("com.squareup.retrofit2:retrofit:2.9.0")

	testImplementation( "junit:junit:4.+")

	androidTestImplementation( "androidx.test.ext:junit:1.1.2")
	androidTestImplementation( "androidx.test.espresso:espresso-core:3.3.0")
}