package com.dougmccluer.words.di


import org.koin.dsl.module


object Di {
	fun modules() = listOf(repositoryModule, viewModelModule, app_module)
	private val viewModelModule = module {

	}
	private val app_module = module {

	}
	private val repositoryModule = module {

	}


}