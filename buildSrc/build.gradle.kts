import org.gradle.kotlin.dsl.`kotlin-dsl`

plugins {
	`kotlin-dsl`
}

repositories {
	jcenter()
}

dependencies {
	implementation(kotlin("script-runtime"))
}